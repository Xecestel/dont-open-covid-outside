extends RichTextLabel

#variables#
export (String, DIR) var fileDir = ""
export (String) var fileName = ""
export (int) var max_line = 1;

var current_line = 0;
var frames = 0;
###########

func _ready() -> void:
	self.set_bbcode(self.readFile());

func _process(delta : float) -> void:
	if (frames == 0):
		if (Input.is_action_pressed("ui_up")):
			current_line = max(0, current_line-1);
			self.scroll_to_line(current_line);
		elif (Input.is_action_pressed("ui_down")):
			current_line = min(max_line, current_line+1);
			self.scroll_to_line(current_line);
		frames = 3;
	frames -= 1;

func readFile() -> String:
	if (fileDir == "" || fileDir == null ||
		fileName == "" || fileName == null):
		return "";
	var filePath = fileDir + "/" + fileName + "_en" + ".txt";
	var f = File.new();
	if !f.file_exists(filePath):
		return "file does not exist: '" + str(filePath) + "'";
	f.open(filePath, File.READ);
	var text = f.get_as_text();
	f.close();
	return text;
