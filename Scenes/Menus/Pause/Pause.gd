extends Control

#constants#
const MAIN_MENU_SCENE = "res://Scenes/Menus/MainMenu/MainMenu.tscn";

#constants#
const MARGIN = 16;

#variables#
var scene : String;
var exiting = false;

onready var resume = self.get_node("PauseRect/CommandsBox/PlayBtn");
onready var title = self.get_node("PauseRect/CommandsBox/TitleBtn");
###########

func _ready() -> void:
	resume.grab_focus();


func _process(delta : float) -> void:
	if (exiting):
		self.get_tree().change_scene(scene);
		self.pause_game();
		
	if (resume.is_hovered()):
		resume.grab_focus();
	if (title.is_hovered()):
		title.grab_focus();

func _input(event : InputEvent) -> void:
	if (event.is_action_pressed("ui_pause")):
		self.pause_game();
		
func pause_game() -> void:
	if (self.visible):
		SoundManager.play_se("PauseOut");
	else:
		SoundManager.play_se("PauseIn");
	get_tree().paused = !get_tree().paused;
	self.set_visible(!self.is_visible());
	resume.grab_focus();
	


#########################
#	SIGNALS HANDLING	#
#########################

func _on_PlayBtn_pressed():
	self.pause_game();

func _on_TitleBtn_pressed():
	SoundManager.play_se("MenuSelect");
	self.exiting = true;
	self.scene = MAIN_MENU_SCENE;
