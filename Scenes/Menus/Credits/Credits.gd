extends Control

#constants#
const MAIN_MENU_PATH	= "res://Scenes/Menus/MainMenu/MainMenu.tscn"
##########

#variables#
onready var back_button = self.get_node("backBtn");
###########


func _ready() -> void:
	back_button.grab_focus();
	
func _process(delta : float) -> void:
	if (Input.is_action_just_pressed("ui_cancel")):
		self.go_back();

func go_back() -> void:
	self.get_tree().change_scene(MAIN_MENU_PATH);


func _on_backBtn_pressed() -> void:
	self.go_back();
