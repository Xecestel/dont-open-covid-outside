extends Node

#constants#
const DEBUG = false;
enum POWERUPS {NONE, MASK, GRANADE, AMMO};
###########

#variables#
var highscore = 0;

onready var controller_connected = Input.is_joy_known(0);
###########

func _ready() -> void:
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed");

# Returns the loot node
func get_loot(id : int) -> Node:
	var loot_node = null
	id = self.normalize_loot_id(id);
	if (id != POWERUPS.NONE):
		var loot_scene = load("res://Scenes/Entities/Powerup/Powerup.tscn");
		loot_node = loot_scene.instance();
		loot_node.set_powerup(id);
	return loot_node;

func normalize_loot_id(id : int) -> int:
	if (id >= 0 && id <= 2):
		id = POWERUPS.NONE;
	elif (id >= 3 && id <= 4):
		id = POWERUPS.MASK;
	elif (id == 5):
		id = POWERUPS.GRANADE;
	elif (id >= 6 && id <= 7):
		id = POWERUPS.AMMO;
	return id;

#################################
#		SETTERS AND GETTERS		#
#################################
# Updates the highscore
func set_highscore(score : int) -> bool:
	if (score > highscore):
		self.highscore = score;
		SaveLoad.save_game();
		return true;
	return false;

# Returns the highscore
func get_highscore() -> int:
	return self.highscore;
	
func is_controller_connected() -> bool:
	return self.controller_connected;

#################################
#		DATA SAVE AND LOAD		#
#################################
# Saves settings on file
func save():
	var save_dict : Dictionary = {	"highscore" : highscore	};
	
	if (save_dict == null):
		print_debug("Error retrieving values for the save dictionary");
		return;
	
	print_debug("Game saved correctly");
	return save_dict;
#end

# Loads settings from file
func load_data(save_file : File) -> bool:
	if (save_file == null):
		return false;

	var current_line = parse_json(save_file.get_line());
	
	for i in current_line.keys():
		self.set(i, current_line[i]);

	return true;


#########################
#	SIGNALS HANDLING	#
#########################

func _on_joy_connection_changed(device : int, connected : bool) -> void:
	if (device == 0):
		controller_connected = connected;
