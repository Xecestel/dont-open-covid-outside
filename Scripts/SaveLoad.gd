extends Node

#constants#
const SAVE_DIR_PATH = "user://Saves/"
const FILE_NAME		= "savegame.save"
##########

#variables#
var game_saved		=	true;
###########

func save_game() -> void:
	game_saved = false;
	var path = SAVE_DIR_PATH + FILE_NAME;
	var save_game	= File.new();
	var save_dir	= Directory.new();
	
	if (save_dir.dir_exists(SAVE_DIR_PATH) == false):
		save_dir.open("user://");
		save_dir.make_dir("Saves");
		
	save_game.open(path, File.WRITE);
	var data = Globals.save();
	save_game.store_line(to_json(data));
	save_game.close();
	game_saved = true;
	
func load_game() -> bool:
	var path = SAVE_DIR_PATH + FILE_NAME;
	var save_game = File.new()
	if (save_game.file_exists(path) == false):
		print_debug("File not found.");
		return false #Error! Nothing to load!
	
	save_game.open(path, File.READ);

	if (Globals.load_data(save_game) == false):
		print_debug("Error occured while loading GlobalVariables");
		return false; #Error loading file
		
	save_game.close();
	return true;
	
	
func file_exists() -> bool:
	var filePath = SAVE_DIR_PATH + FILE_NAME;
	var file = File.new();
	
	return file.file_exists(filePath);
